/**
 * Created by Benjamin on 11/9/2016.
 */

$( document ).ready(function() {
    var changeWidth = $('.crew-member').width();
    $('.crew-row').css({
        'height': changeWidth + 'px'
    });
    $('.crew-member').css({
        'height': changeWidth + 'px'
    })
    $('.page-hover').css({
        'height': changeWidth + 'px'
    })
});
